const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  id: state => state.user.id,
  name: state => state.user.name,
  email: state => state.user.email,
  phoneNumber: state => state.user.phoneNumber,
  sex: state => state.user.sex,
  permission_routes: state => state.permission.routes,
}
export default getters
