import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_template_token'
const TokenName = 'yg_platform_tokenName'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function setTokenName(name) {
  window.sessionStorage.setItem(TokenName, name)
}

export function getTokenName() {
  return window.sessionStorage.getItem(TokenName)
}