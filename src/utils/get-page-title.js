import defaultSettings from '@/settings'

const title = defaultSettings.title || 'frame-web-master'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
