import {
  byteLength
} from "@/utils/index";
const validatName = (rule, value, callback) => {
  if (byteLength(value) < 2 || byteLength(value) > 32) {
    callback(new Error('请输入2-32位英文字符或1-16位中文字符'))
  } else {
    callback()
  }
}
const validAlphabetsASNum = (rule, value, callback) => {
  if (!/^[a-zA-Z0-9_]+$/.test(value)) {
    callback(new Error('只能输入英文、数字、下划线'))
  } else {
    callback()
  }
}
export const optionParent = {
  height: 'auto',
  calcHeight: 95,
  columBtn: false,
  searchShowBtn: false,
  refreshBtn: false,
  tip: false,
  searchShow: true,
  searchMenuSpan: 4,
  searchBtnText: '',
  rowKey: "dictId",
  emptyBtn: false,
  border: true,
  index: true,
  selection: true,
  viewBtn: true,
  menuWidth: 140,
  dialogWidth: 880,
  dialogClickModal: false,
  column: [{
      label: "字典类型",
      prop: "dictType",
      search: true,
      span: 24,
      searchSpan: 10,
      rules: [{
          required: true,
          message: "请输入字典类型",
          trigger: "blur"
        },
        {
          validator: validAlphabetsASNum,
          trigger: 'blur'
        },
        {
          min: 2,
          max: 32,
          message: "请输入2-32位字符",
          trigger: "blur"
        }
      ]
    },
    {
      label: "字典名称",
      prop: "dictName",
      search: true,
      align: "center",
      searchSpan: 10,
      rules: [{
          required: true,
          message: "请输入字典名称",
          trigger: "blur"
        },
        {
          validator: validatName,
          trigger: 'blur'
        }
      ]
    },
    {
      label: "字典备注",
      prop: "remark",
      hide: true
    }
  ]
};

export const optionChild = {
  height: 'auto',
  calcHeight: 95,
  columBtn: false,
  searchShowBtn: false,
  refreshBtn: false,
  tip: false,
  searchShow: true,
  searchMenuSpan: 4,
  tree: true,
  border: true,
  index: true,
  searchBtnText: '',
  emptyBtn: false,
  selection: true,
  viewBtn: false,
  menuWidth: 140,
  dialogWidth: 880,
  dialogClickModal: false,
  column: [{
      label: "字典类型",
      prop: "dictType",
      addDisabled: true,
      editDisabled: true,
      // search: true,
      searchSpan: 10,
      span: 24,
      rules: [{
        required: true,
        message: "请在左侧表格中点击选择上级字典",
        trigger: "blur"
      }]
    },
    {
      label: "字典标签",
      prop: "dictLabel",
      search: true,
      align: "center",
      searchSpan: 10,
      rules: [{
          required: true,
          message: "请输入字典名标签",
          trigger: "blur"
        },
        {
          validator: validatName,
          trigger: "blur"
        }
      ]
    },
    {
      label: "上级字典",
      prop: "parentId",
      // type: "tree",
      // dicData: [],
      hide: true,
      // props: {
      //   label: "title"
      // },
      addDisabled: true,
      editDisabled: true,
      rules: [{
        required: false,
        message: "请在左侧表格中点击选择上级字典",
        trigger: "click"
      }]
    },
    {
      label: "字典键值",
      prop: "dictValue",
      width: 80,
      rules: [{
          required: true,
          message: "请输入字典键值",
          trigger: "blur"
        },
        {
          validator: validAlphabetsASNum,
          trigger: "blur"
        },
        {
          min: 1,
          max: 32,
          message: "请输入2-32位字符",
          trigger: "blur"
        }
      ]
    },
    {
      label: "字典排序",
      prop: "dictSort",
      type: "number",
      align: "right",
      hide: true,
      min: 0,
      max: 9999,
      rules: [{
        required: true,
        message: "请输入字典排序",
        trigger: "blur"
      }]
    },
    // {
    //   label: "封存",
    //   prop: "isSealed",
    //   type: "switch",
    //   align: "center",
    //   width: 80,
    //   dicData: [{
    //       label: "否",
    //       value: 0
    //     },
    //     {
    //       label: "是",
    //       value: 1
    //     }
    //   ],
    //   value: 0,
    //   slot: true,
    //   rules: [{
    //     required: true,
    //     message: "请选择封存",
    //     trigger: "blur"
    //   }]
    // },
    // {
    //   label: "字典备注",
    //   prop: "remark",
    //   hide: true
    // }
  ]
};
