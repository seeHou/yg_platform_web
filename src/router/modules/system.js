/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const systemRouter = {
  path: '/system',
  component: Layout,
  redirect: 'noRedirect',
  name: 'system',
  alwaysShow: true, //一直显示根路由
  meta: {
    title: '系统管理',
    icon: 'nested'
  },
  children: [{
      path: '/system/index',
      component: () => import('@/views/system/dict'),
      name: 'dict',
      meta: {
        title: '字典管理',
        icon: 'el-icon-s-order'
      }
    },
    {
      path: '/system/menu',
      component: () => import('@/views/system/menu'),
      name: 'menu',
      meta: {
        title: '菜单管理',
        icon: 'el-icon-menu'
      }
    },
    {
      path: '/system/user',
      component: () => import('@/views/system/user'),
      name: 'user',
      meta: {
        title: '用户管理',
        icon: 'el-icon-s-custom'
      }
    },
    {
      path: '/system/role',
      component: () => import('@/views/system/role'),
      name: 'role',
      meta: {
        title: '角色管理',
        icon: 'el-icon-s-check'
      }
    },
    {
      path: '/system/operation',
      component: () => import('@/views/system/operation'),
      name: 'operation',
      meta: {
        title: '操作日志',
        icon: 'el-icon-s-check'
      }
    }
    // , {
    //   path: '/system/dept',
    //   component: () => import('@/views/system/dept'),
    //   name: 'dept',
    //   meta: {
    //     title: '部门管理',
    //     icon: 'el-icon-s-check'
    //   }
    // },
  ]
}

export default systemRouter
