/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const mallManageRouter = {
  path: '/mallManage',
  component: Layout,
  redirect: 'noRedirect',
  name: 'mallManage',
  alwaysShow: true, // 一直显示根路由
  meta: {
    title: '店铺管理',
    icon: 'nested'
  },
  children: [
    {
      path: '/mallManage/goods',
      component: () => import('@/views/mallManage/goods'),
      name: 'mallManageGoods',
      meta: {
        title: '发货管理',
        icon: 'el-icon-s-order'
      }
    },
    {
      path: '/mallManage/approve',
      component: () => import('@/views/mallManage/approve'),
      name: 'mallManageApprove',
      meta: {
        title: '店铺审批',
        icon: 'el-icon-menu'
      }
    },
    {
      path: '/mallManage/commodity',
      component: () => import('@/views/mallManage/commodity'),
      name: 'mallManageCommodity',
      meta: {
        title: '店铺审批',
        icon: 'el-icon-menu'
      }
    }

  ]
}

export default mallManageRouter
