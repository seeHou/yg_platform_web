import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/sport/spoBuVenue/list',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function add(row) {
  return request({
    url: '/sport/spoBuVenue/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/sport/spoBuVenue/update',
    method: 'post',
    data: row

  })
}
export function remove(ids) {
  return request({
    url: '/sport/spoBuVenue/delete',
    method: 'post',
    data: ids

  })
}

export function auth(row) {
  return request({
    url: '/sport/spoBuVenue/userVenAuthorization',
    method: 'post',
    data: row
  })
}
