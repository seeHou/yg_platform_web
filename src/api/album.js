import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/sport/spoBuAlbum/list',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function add(row) {
  return request({
    url: '/sport/spoBuAlbum/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/sport/spoBuAlbum/update',
    method: 'post',
    data: row

  })
}
export function remove(ids) {
  return request({
    url: '/sport/spoBuAlbum/delete',
    method: 'post',
    data: ids

  })
}
