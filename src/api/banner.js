import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/sport/spoBuBanner/list',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function add(row) {
  return request({
    url: '/sport/spoBuBanner/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/sport/spoBuBanner/update',
    method: 'post',
    data: row

  })
}
export function remove(ids) {
  return request({
    url: '/sport/spoBuBanner/delete',
    method: 'post',
    data: ids

  })
}
