import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/sport/spoBuLabel/list',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function add(row) {
  return request({
    url: '/sport/spoBuLabel/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/sport/spoBuLabel/update',
    method: 'post',
    data: row

  })
}
export function remove(ids) {
  return request({
    url: '/sport/spoBuLabel/delete',
    method: 'post',
    data: ids

  })
}
