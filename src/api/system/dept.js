import request from '@/utils/request'

export function getPage(params) {
  return request({
    url: '/system/dept/getDeptPage',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function getLazyList(params) {
  return request({
    url: '/system/dept/listDept',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function remove(ids) {
  return request({
    url: '/system/dept/delete',
    method: 'post',
    data: ids

  })
}
export function add(row) {
  return request({
    url: '/system/dept/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/system/dept/update',
    method: 'post',
    data: row

  })
}
