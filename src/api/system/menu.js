import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/system/menu/listMenu',
    method: 'post',
    data: {
      ...params
    }
  })
}
export function remove(ids) {
  return request({
    url: '/system/menu/delete',
    method: 'post',
    data: ids

  })
}
export function add(row) {
  return request({
    url: '/system/menu/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/system/menu/update',
    method: 'post',
    data: row

  })
}
