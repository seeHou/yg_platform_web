import request from '@/utils/request'

export function getRegionList(params) {
  return request({
    url: '/mock/5/system/region/list',
    method: 'post',
    data: {
      ...params
    }
  })
}
