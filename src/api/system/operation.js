import request from '@/utils/request'

export function dict_biz(codes) {
  return request({
    url: '/system/dictData/getDictBizList',
    method: 'get',
    params: {
      codes
    }
  })
}
export function operationLogList(dto) {
  return request({
    url: '/system/operLog/list',
    method: 'post',
    data: {
      ...dto
    }

  })
}
