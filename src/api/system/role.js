import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/system/role/getRolePage',
    method: 'post',
    data: {
      ...params
    }
  })
}
export function grantTree() {
  return request({
    url: '/system/role/listRole',
    method: 'post',
  })
}
export function grant(roleId, menuIds) {
  return request({
    url: '/system/role/roleMenuAuthorization',
    method: 'post',
    data: {
      roleId,
      menuIds
    }
  })
}
export function remove(ids) {
  return request({
    url: '/system/role/delete',
    method: 'post',
    data: ids

  })
}
export function add(row) {
  return request({
    url: '/system/role/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/system/role/update',
    method: 'post',
    data: row

  })
}
export function getRole(roleId) {
  return request({
    url: '/system/role/roleMenu',
    method: 'get',
    params: {
      roleId
    }
  })
}
export function getRoleTree() {
  return request({
    url: '/system/role/listRole',
    method: 'post',
  })
}
