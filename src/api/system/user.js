import request from '@/utils/request'

export function getList(page, params) {
  return request({
    url: '/system/user/getUserPage',
    method: 'post',
    data: {
      // query: {
      ...params,
      // },
      ...page
    }
  })
}
export function remove(ids) {
  return request({
    url: '/system/user/delete',
    method: 'post',
    data: ids

  })
}
export function add(row) {
  return request({
    url: '/system/user/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/system/user/update',
    method: 'post',
    data: row
  })
}
export function resetPassword(userId) {
  return request({
    url: '/system/user/resetPassword',
    method: 'post',
    data: {
      userId
    }
  })
}
