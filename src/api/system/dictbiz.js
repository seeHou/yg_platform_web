import request from '@/utils/request'

export function getParentList(current, size, dictCode, dictName) {
  return request({
    url: '/system/dictType/getDictTypePage',
    method: 'post',
    data: {

      current,
      size,

      // query: {
      dictCode,
      dictName
      // }
    }
  })
}
export function getChildList(dictType, dictName) {
  return request({
    url: '/system/dictData/listDictData',
    method: 'post',
    data: {
      dictType,
      dictName
    }
  })
}

export function remove(ids) {
  return request({
    url: '/system/dictType/delete',
    method: 'post',
    data: ids

  })
}
export function removechild(ids) {
  return request({
    url: '/system/dictData/delete',
    method: 'post',
    data: ids

  })
}
export function add(dictName, dictType, remark) {
  return request({
    url: '/system/dictType/add',
    method: 'post',
    data: {
      dictName,
      dictType,
      remark
    }
  })
}
export function addChild(dictLabel, dictSort, dictValue, dictType) {
  return request({
    url: '/system/dictData/add',
    method: 'post',
    data: {
      dictLabel,
      dictSort,
      dictValue,
      dictType
    }
  })
}
export function update(dictId, dictName, dictType, remark) {
  return request({
    url: '/system/dictType/update',
    method: 'post',
    data: {
      dictId,
      dictName,
      dictType,
      remark
    }
  })
}
export function updatechild(dictCode, dictLabel, dictSort, dictValue, dictType) {
  return request({
    url: '/system/dictData/update',
    method: 'post',
    data: {
      dictCode,
      dictLabel,
      dictSort,
      dictValue,
      dictType,
    }
  })
}
