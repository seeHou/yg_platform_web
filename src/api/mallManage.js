import request from '@/utils/request'

// 待发货商品订单列表
export function getOrderList(params) {
  return request({
    url: '/admin/order/getOrderList',
    method: 'post',
    data: {
      ...params
    }
  })
}

// 修改订单发货状态
export function updateOrderGoodsSendStatus(params) {
  return request({
    url: '/admin/order/updateOrderGoodsSendStatus',
    method: 'post',
    data: {
      ...params
    }
  })
}

// 订单详情
export function getOrderGoodsDetails(params) {
  return request({
    url: '/admin/order/getOrderGoodsDetails',
    method: 'post',
    data: {
      ...params
    }
  })
}

// 待审核的门店列表
export function storeList(params) {
  return request({
    url: '/admin/store/storeList',
    method: 'post',
    data: {
      ...params
    }
  })
}

// 审核门店
export function applyStore(params) {
  return request({
    url: '/admin/store/applyStore',
    method: 'post',
    data: {
      ...params
    }
  })
}

// 商品列表
export function goodsList(params) {
  return request({
    url: '/admin/goods/goodsList',
    method: 'post',
    data: {
      ...params
    }
  })
}

// 商品修改上下架状态
export function changeStatus(params) {
  return request({
    url: '/admin/goods/changeStatus',
    method: 'post',
    data: {
      ...params
    }
  })
}

// 商品分类列表
export function goodsCategoryList(params) {
  return request({
    url: '/admin/goods/goodsCategoryList',
    method: 'post',
    data: {
      ...params
    }
  })
}

// 新增商品
export function addGoods(params) {
  return request({
    url: '/admin/goods/addGoods',
    method: 'post',
    data: {
      ...params
    }
  })
}
// 编辑商品
export function updateGoods(params) {
  return request({
    url: '/admin/goods/updateGoods',
    method: 'post',
    data: {
      ...params
    }
  })
}

// 商品详情接口
export function goodsInfo(params) {
  return request({
    url: '/admin/goods/goodsInfo',
    method: 'post',
    data: {
      ...params
    }
  })
}
