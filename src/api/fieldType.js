import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/sport/spoBuFieldType/list',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function add(row) {
  return request({
    url: '/sport/spoBuFieldType/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/sport/spoBuFieldType/update',
    method: 'post',
    data: row

  })
}
export function remove(ids) {
  return request({
    url: '/sport/spoBuFieldType/delete',
    method: 'post',
    data: ids

  })
}
