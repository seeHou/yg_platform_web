import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/system/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    headers: {
      'admin-token-admin': token,
      'user-type': 'value-admin'
    },
    url: '/system/user/userInfo',
    method: 'get',
    params: {
      token
    }
  })
}

export function logout(token) {
  return request({
    headers: {
      'admin-token-admin': token,
    },
    url: '/system/user/logout',
    method: 'post'
  })
}
export function editPassword(data) {
  return request({
    url: '/system/user/editPassword',
    method: 'post',
    data
  })
}
export function uploadImg(file, fileName) {
  return request({
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    url: '/system/oss/putFile',
    method: 'post',
    params: {
      fileName
    },
    data: file
  })
}
