import request from '@/utils/request'

export function getVenList(params) {
  return request({
    url: '/sport/spoBuOrder/venList',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function getClassList(params) {
  return request({
    url: '/sport/spoBuOrder/classList',
    method: 'post',
    data: {
      ...params
    }
  })
}
