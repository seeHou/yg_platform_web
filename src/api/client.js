import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/sport/spoBuUser/list',
    method: 'post',
    data: {
      ...params
    }
  })
}
