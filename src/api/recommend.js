import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/sport/spoBuRecommend/list',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function add(row) {
  return request({
    url: '/sport/spoBuRecommend/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/sport/spoBuRecommend/update',
    method: 'post',
    data: row

  })
}
export function remove(ids) {
  return request({
    url: '/sport/spoBuRecommend/delete',
    method: 'post',
    data: ids

  })
}
