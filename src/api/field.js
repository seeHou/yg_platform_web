import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/sport/spoBuField/list',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function add(row) {
  return request({
    url: '/sport/spoBuField/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/sport/spoBuField/update',
    method: 'post',
    data: row

  })
}
export function remove(ids) {
  return request({
    url: '/sport/spoBuField/delete',
    method: 'post',
    data: ids

  })
}
