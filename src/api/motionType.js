import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/sport/spoBuMotionType/list',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function add(row) {
  return request({
    url: '/sport/spoBuMotionType/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/sport/spoBuMotionType/update',
    method: 'post',
    data: row

  })
}
export function remove(ids) {
  return request({
    url: '/sport/spoBuMotionType/delete',
    method: 'post',
    data: ids

  })
}
