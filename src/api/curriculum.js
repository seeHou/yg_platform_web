import request from '@/utils/request'

export function getLazyList(params) {
  return request({
    url: '/sport/spoBuCurriculum/list',
    method: 'post',
    data: {
      ...params
    }
  })
}

export function add(row) {
  return request({
    url: '/sport/spoBuCurriculum/add',
    method: 'post',
    data: row

  })
}
export function update(row) {
  return request({
    url: '/sport/spoBuCurriculum/update',
    method: 'post',
    data: row

  })
}
export function remove(ids) {
  return request({
    url: '/sport/spoBuCurriculum/delete',
    method: 'post',
    data: ids

  })
}
